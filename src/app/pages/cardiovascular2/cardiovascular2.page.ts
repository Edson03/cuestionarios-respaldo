import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cardiovascular2',
  templateUrl: './cardiovascular2.page.html',
  styleUrls: ['./cardiovascular2.page.scss'],
})
export class Cardiovascular2Page implements OnInit {

  constructor(public alertCtrl: AlertController, private router: Router) { }
  cont:number=0;
  card2:string="";
  data =[
    {
      label: ' ... tiene pulsos periféricos disminuidos? ',
      selected: false,
      value: 2,
      name: 'tiene pulsos periféricos disminuidos. '
      }
  ]
  parametros={
    pregunta:'',
    valor:0,
  }

  ngOnInit() {
  }

  Change(){  
    this.cont = 0;
    this.card2=""; 
    for (var x=0; x < this.data.length; x++) {
      if (this.data[x].selected==true) {
        this.cont += 1;
        this.card2=this.card2+this.data[x].name;
      }
    }
    
    if(this.cont>0){
      let Storage = JSON.parse(sessionStorage.getItem("Pregunta_cardiovascular"));
      if(Storage.valor<=2){
       this.parametros.valor=2;
      }else{
        this.parametros.valor=Storage.valor;
      }
      this.parametros.pregunta=Storage.pregunta+" El paciente: "+this.card2;
      sessionStorage.setItem("Pregunta_cardiovascular", JSON.stringify(this.parametros));
      this.router.navigate(['/respiratorio4']);
     }
  }

  async Siguiente(){
    let cardiovascular=JSON.parse(sessionStorage.getItem("Pregunta_cardiovascular"));
    if(cardiovascular.valor==1){
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Alerta',
        subHeader:'Evaluación Cardiovascular',
        message: 'El paciente presenta un estado Cardiovascular LEVE debido a su Frecuencia Cardíaca o Llenado Capilar por lo que se procederá con la evaluación Respiratoria.',
        buttons: ['OK']
     });
     await alert.present();
      this.router.navigate(['/respiratorio4']);
    }else{
      this.router.navigate(['/cardiovascular1']);
    }
    
  }

}
