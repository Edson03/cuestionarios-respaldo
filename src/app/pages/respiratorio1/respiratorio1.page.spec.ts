import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Respiratorio1Page } from './respiratorio1.page';

describe('Respiratorio1Page', () => {
  let component: Respiratorio1Page;
  let fixture: ComponentFixture<Respiratorio1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Respiratorio1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Respiratorio1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
