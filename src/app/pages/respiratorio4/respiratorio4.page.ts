import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-respiratorio4',
  templateUrl: './respiratorio4.page.html',
  styleUrls: ['./respiratorio4.page.scss'],
})
export class Respiratorio4Page implements OnInit {
  respiratorio={
    frespiratoria:'',
    nebulizaciones:'',
    tnebulizaciones:'',
    saturacion:'',
    oxigeno:'',
    valor:0,
    estado:'',
  }
  parametros={
    pregunta:'',
    valor:0,
  }
  estadofrecuencia:string=""; valorfrecuencia:number=0;
  estadoneb:string="";       valorneb:number=0;
  estadosat:string="";      valorsat:number=0;
   inicial:number=0; final:number=0;
   aux:number=0;
   estado:string="";
                          //Edad   Normal,  Leve,    Moderada Severa
                        //       min-max, min-max, min-max, max
frecuencia:number[][] =[ [ 1,2,   30,56,  57,62,   63,76,   77],
                         [ 3,5,   28,52,  53,58,   59,71,   72],
                         [ 6,8,   26,49,  50,54,   55,67,   68],
                         [ 9,11,  24,46,  47,51,   52,63,   64],
                         [12,17,  23,43,  44,48,   49,60,   61],
                         [18,23,  21,40,  41,45,   46,57,   58],
                        //Edad   Normal,  Leve,    Moderada Severa
                        //       min-max, min-max, min-max, max
                         [ 1,1,   23,43,  44,48,   49,60,   61],
                         [ 2,2,   20,37,  38,42,   43,54,   55],
                         [ 3,3,   19,35,  36,40,   41,52,   53],
                         [ 4,5,   18,33,  34,37,   38,50,   51],
                         [ 6,7,   17,31,  32,35,   36,46,   47],
                         [ 8,11,  16,28,  29,31,   32,41,   42],
                         [12,14,  15,25,  26,28,   29,35,   36],
                         [15,18,  14,23,  24,26,   27,32,   32], //Valor a verificar
                         [18,150, 12,20,  21,24,   25,29,   30],
]

  constructor(public alertCtrl: AlertController,private router: Router) {
    this.Frec;
    this.onClick;
   }

  ngOnInit() {
    this.respiratorio;
  }

  Frec(){
    let paciente = JSON.parse(sessionStorage.getItem("Paciente"));
     if(paciente.tipoedad=="años"){
      this.inicial=6;
      this.final=14;
    } else if(paciente.tipoedad=="meses"){
      this.inicial=0; 
      this.final=5;
    }
    for(var f:number = this.inicial; f<=this.final; f++){
      if(parseInt(paciente.edad)>=this.frecuencia[f][0]&&parseInt(paciente.edad)<=this.frecuencia[f][1]){
        this.aux=0;
        for(var c:number=2; c<=6;c+=2){
          if(parseInt(this.respiratorio.frespiratoria)>=this.frecuencia[f][c]&&parseInt(this.respiratorio.frespiratoria)<=this.frecuencia[f][c+1]){
            this.valorfrecuencia=this.aux;
          }else if(parseInt(this.respiratorio.frespiratoria)>=this.frecuencia[f][8]){
            this.valorfrecuencia=3;
          }
          this.aux++; 
        }
      }
    }
    //console.log(this.valorfrecuencia);
    switch(this.valorfrecuencia){
      case 0: return this.estadofrecuencia="NORMAL";
      case 1: return this.estadofrecuencia="LEVE";
      case 2: return this.estadofrecuencia="MODERADA";
      case 3: return this.estadofrecuencia="SEVERA";
      default: return this.estadofrecuencia="";
    }
  }

  onClick(){
    if(this.respiratorio.nebulizaciones=="No"){
      this.respiratorio.tnebulizaciones="0";
    }else{
      this.respiratorio.tnebulizaciones="";
    }
   }

   Tiempo(){
     if(parseInt(this.respiratorio.tnebulizaciones)>4){
       this.estadoneb="SEVERA";
       this.valorneb=3;
     }else if(parseInt(this.respiratorio.tnebulizaciones)==4){
      this.estadoneb="MODERADA";
      this.valorneb=2;
    } else if(parseInt(this.respiratorio.tnebulizaciones)>0 && parseInt(this.respiratorio.tnebulizaciones)<4){
      this.estadoneb="LEVE";
      this.valorneb=1;
    } else{
      this.estadoneb="NORMAL"
      this.valorneb=0;
    }
   }

   Oxigeno(){
    if(parseInt(this.respiratorio.saturacion)<90 && this.respiratorio.oxigeno=="Si"){
      this.estadosat="SEVERA";
      this.valorsat=3;
    }else if(parseInt(this.respiratorio.saturacion)>=88 && parseInt(this.respiratorio.saturacion)<=89 && this.respiratorio.oxigeno=="No"){
     this.estadosat="MODERADA";
     this.valorsat=2;
   }else if(parseInt(this.respiratorio.saturacion)>=90 && parseInt(this.respiratorio.saturacion)<=94 && this.respiratorio.oxigeno=="No"){
     this.estadosat="LEVE";
     this.valorsat=1;
   }else if(parseInt(this.respiratorio.saturacion)>=95 && parseInt(this.respiratorio.saturacion)<=100 ){
     this.estadosat="NORMAL";
     this.valorsat=0;
   }else{
     this.estadosat="";
     this.valorsat=parseInt("");
   }
    //console.log(this.estadosat+ " - "+this.valorsat);
    //console.log(this.respiratorio.saturacion+" - "+this.respiratorio.oxigeno);
     
   }
 
  async Siguiente(){
    if(this.valorsat==3||this.valorfrecuencia==3||this.valorneb==3){
      this.respiratorio.valor=3;
    }else if(this.valorsat==2||this.valorfrecuencia==2||this.valorneb==2){
      this.respiratorio.valor=2;
    }else if(this.valorsat==1||this.valorfrecuencia==1||this.valorneb==1){
      this.respiratorio.valor=1;
    }else if(this.valorsat==0||this.valorfrecuencia==0||this.valorneb==0){
      this.respiratorio.valor=0;
    }

    this.respiratorio.estado=("Frecuencia Respiratoria: "+this.respiratorio.frespiratoria +" resp/min ["+this.estadofrecuencia
                            +"], Nebulizaciones: "+this.respiratorio.nebulizaciones+" Tiempo: "+ this.respiratorio.tnebulizaciones + " hrs [" +this.estadoneb+
                            "], Saturación: "+this.respiratorio.saturacion+ "% Con Oxigeno: "+ this.respiratorio.oxigeno+" ["+this.estadosat)+"].";
    //console.log(this.respiratorio.estado)
    //localStorage.setItem("Local", JSON.stringify(this.respiratorio) );
    this.parametros.pregunta=this.respiratorio.estado;
    this.parametros.valor=this.respiratorio.valor;
    sessionStorage.setItem("Pregunta_respiratoria", JSON.stringify(this.parametros));
    if(this.respiratorio.valor==3){
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Alerta',
        subHeader:'Evaluación Respiratoria',
        message: 'El paciente presenta un estado Respiratorio SEVERO, por lo que se procederá con la siguiente evaluación.',
        buttons: ['OK']
     });
     await alert.present();
      this.router.navigate(['/preguntas-extras']);
    }else{
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Alerta',
        subHeader:'Evaluación Respiratoria',
        message: 'El paciente NO presenta un estado Respiratorio SEVERO, puede continuar con la evaluación.',
        buttons: ['OK']
     });
     await alert.present();
      this.router.navigate(['/respiratorio3']);
    }
  }

  Anterior(){
      this.router.navigate(['/cardiovascular4']);
  }

}
