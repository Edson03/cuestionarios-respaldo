import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-preguntas-extras',
  templateUrl: './preguntas-extras.page.html',
  styleUrls: ['./preguntas-extras.page.scss'],
})
export class PreguntasExtrasPage implements OnInit {
  cont:number=0;  valor:number=0;
  resp:string="";
  public form = [
    {  isChecked: false, item:'Enfermera preocupada, ' },
    {  isChecked: false, item:'Familia preocupada, ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }

  constructor(private router: Router) { }
  
  anterior(){
    this.router.navigate(['/respiratorio4']);
  }

  adelante(){
    sessionStorage.setItem("Pregunta_preocupacion", JSON.stringify(this.parametros));
    this.router.navigate(['/resultados']);
  }

  ngOnInit() {
  }

  preocupacion(){
    this.cont = 0;
    this.resp=""; 
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.resp=this.resp+this.form[x].item;
      }
    }
    //console.log(this.cont);
    //console.log(this.resp);
    this.parametros.valor=this.cont;
    this.parametros.pregunta=this.resp;
  }
}

