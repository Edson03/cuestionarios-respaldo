import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Popinfo1Component } from '../../components/popinfo1/popinfo1.component';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-respiratorio1',
  templateUrl: './respiratorio1.page.html',
  styleUrls: ['./respiratorio1.page.scss'],
})
export class Respiratorio1Page implements OnInit {
  public form = [
    { isChecked: false, item:'Trabajo respiratorio leve. ' },
    { isChecked: false, item:'Oxígeno hasta 1 L en cánula binasal. ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }
  cont:number=0;
  resp1:string="";

  constructor( public alertCtrl: AlertController,private router: Router,private popoverCtrl: PopoverController ) { }

  ngOnInit() {
  }

  async mostrarPop1(evento ) {
    const popover = await this.popoverCtrl.create({
      component: Popinfo1Component,
      event: evento,
      mode: 'ios',
    });
    await popover.present();
  }

  Change(){  
    this.cont = 0;
    this.resp1=""; 
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.resp1=this.resp1+this.form[x].item;
      }
    }
    
    if(this.cont>0){
      let Storage = JSON.parse(sessionStorage.getItem("Pregunta_respiratoria"));
      if(Storage.valor<=1){
       this.parametros.valor=1;
      }else{
        this.parametros.valor=Storage.valor;
      }
      this.parametros.pregunta=Storage.pregunta+" "+this.resp1;
      sessionStorage.setItem("Pregunta_respiratoria", JSON.stringify(this.parametros));
      this.router.navigate(['/preguntas-extras']);
     }
  }

  async Siguiente(){
    let respiratorio=JSON.parse(sessionStorage.getItem("Pregunta_respiratoria"));
    if(respiratorio.valor==0){
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Alerta',
        subHeader:'Evaluación Respiratoria',
        message: 'El paciente presenta un estado Respiratorio NORMAL debido a su Frecuencia Respiratoria o Saturación y Uso de Oxígeno por lo que se procederá con la siguiente evaluación.',
        buttons: ['OK']
     });
     await alert.present();
      this.router.navigate(['/preguntas-extras']);
    }else{
    this.router.navigate(['/respiratorio0']);
    }
  }

}

