import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cardiovascular1',
  templateUrl: './cardiovascular1.page.html',
  styleUrls: ['./cardiovascular1.page.scss'],
})
export class Cardiovascular1Page implements OnInit {
  cont:number=0;
  card1:string="";
  public form = [
    { val: '... está pálido ?',          isChecked: false, item:'está pálido. ' },
    { val: '... está vasodilatado ?',    isChecked: false, item:'está vasodilatado. ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }

  constructor(public alertCtrl: AlertController,private router: Router) { }

  ngOnInit() {
  }

  Change(){  
    this.cont = 0;
    this.card1=""; 
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.card1=this.card1+this.form[x].item;
      }
    }
    
    if(this.cont>0){
      let Storage = JSON.parse(sessionStorage.getItem("Pregunta_cardiovascular"));
      if(Storage.valor<=1){
       this.parametros.valor=1;
      }else{
        this.parametros.valor=Storage.valor;
      }
      this.parametros.pregunta=Storage.pregunta+" El paciente: "+this.card1;
      sessionStorage.setItem("Pregunta_cardiovascular", JSON.stringify(this.parametros));
      this.router.navigate(['/respiratorio4']);
     } 
  }

  async Siguiente(){
    let cardiovascular=JSON.parse(sessionStorage.getItem("Pregunta_cardiovascular"));
    if(cardiovascular.valor==0){
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Alerta',
        subHeader:'Evaluación Cardiovascular',
        message: 'El paciente presenta un estado Cardiovascular NORMAL debido a su Frecuencia Cardíaca o Llenado Capilar por lo que se procederá con la evaluación Respiratoria.',
        buttons: ['OK']
     });
     await alert.present();
      this.router.navigate(['/respiratorio4']);
    }else{
    this.router.navigate(['/cardiovascular0']);
    }
  }
 
}
