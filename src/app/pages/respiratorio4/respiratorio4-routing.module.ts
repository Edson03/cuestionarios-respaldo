import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Respiratorio4Page } from './respiratorio4.page';

const routes: Routes = [
  {
    path: '',
    component: Respiratorio4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Respiratorio4PageRoutingModule {}
