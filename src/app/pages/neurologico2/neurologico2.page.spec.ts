import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Neurologico2Page } from './neurologico2.page';

describe('Neurologico2Page', () => {
  let component: Neurologico2Page;
  let fixture: ComponentFixture<Neurologico2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Neurologico2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Neurologico2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
