import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Neurologico2Page } from './neurologico2.page';

const routes: Routes = [
  {
    path: '',
    component: Neurologico2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Neurologico2PageRoutingModule {}
