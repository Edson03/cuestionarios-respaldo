import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Cardiovascular4PageRoutingModule } from './cardiovascular4-routing.module';

import { Cardiovascular4Page } from './cardiovascular4.page';
//import { Module } from 'module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Cardiovascular4PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Cardiovascular4Page]
})
export class Cardiovascular4PageModule {}
