import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatosPacientesPage } from './datos-pacientes.page';

const routes: Routes = [
  {
    path: '',
    component: DatosPacientesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatosPacientesPageRoutingModule {}
