import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Cardiovascular1Page } from './cardiovascular1.page';

const routes: Routes = [
  {
    path: '',
    component: Cardiovascular1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Cardiovascular1PageRoutingModule {}
