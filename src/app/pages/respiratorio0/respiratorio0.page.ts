import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-respiratorio0',
  templateUrl: './respiratorio0.page.html',
  styleUrls: ['./respiratorio0.page.scss'],
})
export class Respiratorio0Page implements OnInit {
  cont:number=0;  valor:number=0;
  resp0:string="";
  public form = [
    {  isChecked: false, item:'Sin retracciones. ' },
    {  isChecked: false, item:'Patrón respiratorio normal o en su basal. ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }

  constructor(public alertCtrl: AlertController,private router: Router) { 
  }

  ngOnInit() {
  }

  Change(){  
    this.cont = 0;
    this.resp0=""; 
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.resp0=this.resp0+this.form[x].item;
      }
    }
    
    if(this.cont>0){
      let Storage = JSON.parse(sessionStorage.getItem("Pregunta_respiratoria"));
      if(Storage.valor>0){
       this.parametros.valor=Storage.valor;
      }else{
        this.parametros.valor=0;
      }
      this.parametros.pregunta=Storage.pregunta+" "+this.resp0;
      sessionStorage.setItem("Pregunta_respiratoria", JSON.stringify(this.parametros));
      this.router.navigate(['/preguntas-extras']);
     }
  }

  async Siguiente(){
    const alert = await this.alertCtrl.create({
      backdropDismiss: false,
      header: 'Alerta',
      subHeader: 'Usted no selecciono ninguna opción',
      message: 'Por favor, seleccione por lo menos una opción de la evaluación Respiratoria.',
      buttons: ['OK']
    });
    await alert.present();
  }
}
