import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreguntasExtrasPage } from './preguntas-extras.page';

const routes: Routes = [
  {
    path: '',
    component: PreguntasExtrasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreguntasExtrasPageRoutingModule {}
