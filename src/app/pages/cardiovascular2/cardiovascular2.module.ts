import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Cardiovascular2PageRoutingModule } from './cardiovascular2-routing.module';

import { Cardiovascular2Page } from './cardiovascular2.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Cardiovascular2PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Cardiovascular2Page]
})
export class Cardiovascular2PageModule {}
