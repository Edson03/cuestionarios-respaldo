import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cardiovascular0',
  templateUrl: './cardiovascular0.page.html',
  styleUrls: ['./cardiovascular0.page.scss'],
})
export class Cardiovascular0Page implements OnInit {
  cont:number=0;  valor:number=0;
  card0:string="";
  public form = [
    { val: '... tiene coloración adecuada de la piel?', isChecked: false, item:'tiene coloración adecuada de la piel. ' },
    { val: '... tiene pulsos periféricos normales?',    isChecked: false, item:'tiene pulsos periféricos normales. ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }

  constructor(public alertCtrl: AlertController,private router: Router) {
   }

  ngOnInit() {
   }

  Change(){  
    this.cont = 0;
    this.card0=""; 
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.card0=this.card0+this.form[x].item;
      }
    }

    if(this.cont>0){
      let Storage = JSON.parse(sessionStorage.getItem("Pregunta_cardiovascular"));
      if(Storage.valor>=0){
       this.parametros.valor=Storage.valor;
      }else{
        this.parametros.valor=0;
      }
      this.parametros.pregunta=Storage.pregunta+" El paciente: "+this.card0;
      sessionStorage.setItem("Pregunta_cardiovascular", JSON.stringify(this.parametros));
      this.router.navigate(['/respiratorio4']);
     }    
  }


  async Siguiente(){
    const alert = await this.alertCtrl.create({
      backdropDismiss: false,
      header: 'Alerta',
      subHeader: 'Usted no selecciono ninguna opción',
      message: 'Por favor, seleccione una opción de la evaluación Cardiovascular.',
      buttons: ['OK']
    });
    await alert.present();
  }

}
