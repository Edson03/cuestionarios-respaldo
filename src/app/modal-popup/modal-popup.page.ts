import { Component, Input } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { NavParams,ModalController } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-modal-popup',
  templateUrl: './modal-popup.page.html',
  styleUrls: ['./modal-popup.page.scss'],
})
export class ModalPopupPage  {
  correo:string=""

  constructor(public alertCtrl: AlertController,public navParams: NavParams,public modalCtrl: ModalController, public http: HttpClient) {
    console.log(navParams.get('name'));
   }

   public closeModal() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  EnviarSolicitud(){
    this.http.get<any[]>(`https://localhost:44347/api/recovery/${this.correo}`).subscribe(dataPersonas=>{
      if(dataPersonas==null){
        console.log('No se encontro el correo en la base de datos');
        this.NotFound();
      }else{
        this.Enviar();
      }
    });
  }

  async Enviar(){
        const alert = await this.alertCtrl.create({
      backdropDismiss: false,
      header: 'Restablecer Contraseña',
      subHeader:'Olvidaste tu contraseña',
      message: 'Ingrese a su correo electrónico para seguir las instrucciones de recuperación de contraseña.',
      buttons: ['OK']
   });
   await alert.present();
   this.closeModal();
  }

  async NotFound(){
    const alert = await this.alertCtrl.create({
  backdropDismiss: false,
  header: 'Restablecer Contraseña',
  subHeader:'Olvidaste tu contraseña',
  message: 'El correo ingresado no esta registrado en la base de datos de EVAT.',
  buttons: ['OK']
});
await alert.present();
//this.closeModal();
this.correo="";
}

}
