import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Respiratorio2Page } from './respiratorio2.page';

const routes: Routes = [
  {
    path: '',
    component: Respiratorio2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Respiratorio2PageRoutingModule {}
