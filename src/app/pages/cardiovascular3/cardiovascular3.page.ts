import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cardiovascular3',
  templateUrl: './cardiovascular3.page.html',
  styleUrls: ['./cardiovascular3.page.scss'],
})
export class Cardiovascular3Page implements OnInit {
  cont:number=0;
  card3:string="";

  constructor(public alertCtrl: AlertController,private router: Router) { }
  data =[
    {
      label: ' ... está marmóreo? ',
      selected: false,
      value: 3,
      name: 'está marmóreo. '
    },
    {
      label: '... tiene bradicardia sintomática?',
      selected: false,
      value: 3,
      name: 'tiene bradicardia sintomática. '
    },
    {
      label: '... tiene arritmias?',
      selected: false,
      value: 3,
      name: 'tiene arritmias. '
    }
]

  parametros={
    pregunta:'',
    valor:0,
  }
  ngOnInit() {
    this.data;
  }

  Change(){  
    this.cont = 0;
    this.card3=""; 
    for (var x=0; x < this.data.length; x++) {
      if (this.data[x].selected==true) {
        this.cont += 1;
        this.card3=this.card3+this.data[x].name;
      }
    }
    
    if(this.cont>0){
     let Storage = JSON.parse(sessionStorage.getItem("Pregunta_cardiovascular"));
      this.parametros.valor=3;
     this.parametros.pregunta=Storage.pregunta+" El paciente: "+this.card3;
     sessionStorage.setItem("Pregunta_cardiovascular", JSON.stringify(this.parametros));
     this.router.navigate(['/respiratorio4']);
    }
  }

  async Siguiente(){
    let cardiovascular=JSON.parse(sessionStorage.getItem("Pregunta_cardiovascular"));
    if(cardiovascular.valor==2){
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Alerta',
        subHeader:'Evaluación Cardiovascular',
        message: 'El paciente presenta un estado Cardiovascular MODERADO debido a su Frecuencia Cardíaca o Llenado Capilar por lo que se procederá con la evaluación Respiratoria.',
        buttons: ['OK']
     });
     await alert.present();
      this.router.navigate(['/respiratorio4']);
    }else{
    this.router.navigate(['/cardiovascular2']);
    }
  }

}
