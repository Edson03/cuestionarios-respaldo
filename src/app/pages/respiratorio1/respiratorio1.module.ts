import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Respiratorio1PageRoutingModule } from './respiratorio1-routing.module';

import { Respiratorio1Page } from './respiratorio1.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Respiratorio1PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Respiratorio1Page]
})
export class Respiratorio1PageModule {}
