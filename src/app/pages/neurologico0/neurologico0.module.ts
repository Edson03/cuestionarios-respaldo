import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Neurologico0PageRoutingModule } from './neurologico0-routing.module';

import { Neurologico0Page } from './neurologico0.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Neurologico0PageRoutingModule
  ],
  declarations: [Neurologico0Page]
})
export class Neurologico0PageModule {}
