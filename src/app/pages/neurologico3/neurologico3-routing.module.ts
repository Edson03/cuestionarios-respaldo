import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Neurologico3Page } from './neurologico3.page';

const routes: Routes = [
  {
    path: '',
    component: Neurologico3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Neurologico3PageRoutingModule {}
