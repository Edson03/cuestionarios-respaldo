import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreguntasExtrasPageRoutingModule } from './preguntas-extras-routing.module';

import { PreguntasExtrasPage } from './preguntas-extras.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreguntasExtrasPageRoutingModule
  ],
  declarations: [PreguntasExtrasPage]
})
export class PreguntasExtrasPageModule {}
