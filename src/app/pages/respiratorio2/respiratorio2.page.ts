import { Component, OnInit } from '@angular/core';
import { Popinfo2Component } from '../../components/popinfo2/popinfo2.component';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-respiratorio2',
  templateUrl: './respiratorio2.page.html',
  styleUrls: ['./respiratorio2.page.scss'],
})
export class Respiratorio2Page implements OnInit {
  public form = [
    { isChecked: false, item:'Trabajo respiratorio moderado. ' },
    { isChecked: false, item:'Oxígeno de 1 a 3 L en cánula binasal. ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }
  cont:number=0;
  resp2:string="";

  constructor(public alertCtrl: AlertController,private router: Router,private popoverCtrl: PopoverController) { }

  ngOnInit() {
  }

  async mostrarPop2( evento2 ) {
    const popover = await this.popoverCtrl.create({
      component: Popinfo2Component,
      event: evento2,
      mode: 'ios',
    });
    await popover.present();
  }

  Change(){  
    this.cont = 0;
    this.resp2=""; 
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.resp2=this.resp2+this.form[x].item;
      }
    }
    
    if(this.cont>0){
      let Storage = JSON.parse(sessionStorage.getItem("Pregunta_respiratoria"));
      if(Storage.valor<=2){
       this.parametros.valor=2;
      }else{
        this.parametros.valor=Storage.valor;
      }
      this.parametros.pregunta=Storage.pregunta+" "+this.resp2;
      sessionStorage.setItem("Pregunta_respiratoria", JSON.stringify(this.parametros));
      this.router.navigate(['/preguntas-extras']);
     }
  }

  async Siguiente(){
    let respiratorio=JSON.parse(sessionStorage.getItem("Pregunta_respiratoria"));
    if(respiratorio.valor==1){
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Alerta',
        subHeader:'Evaluación Respiratoria',
        message: 'El paciente presenta un estado Respiratorio LEVE debido a su Frecuencia Respiratoria o Saturación y Uso de Oxígeno por lo que se procederá con la siguiente evaluación.',
        buttons: ['OK']
     });
     await alert.present();
      this.router.navigate(['/preguntas-extras']);
    }else{
    this.router.navigate(['/respiratorio1']);
    }
  }

}
