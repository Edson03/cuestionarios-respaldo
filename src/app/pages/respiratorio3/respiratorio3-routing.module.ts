import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Respiratorio3Page } from './respiratorio3.page';

const routes: Routes = [
  {
    path: '',
    component: Respiratorio3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Respiratorio3PageRoutingModule {}
