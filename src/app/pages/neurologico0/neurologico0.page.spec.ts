import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Neurologico0Page } from './neurologico0.page';

describe('Neurologico0Page', () => {
  let component: Neurologico0Page;
  let fixture: ComponentFixture<Neurologico0Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Neurologico0Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Neurologico0Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
