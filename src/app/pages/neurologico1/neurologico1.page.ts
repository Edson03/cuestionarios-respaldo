import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-neurologico1',
  templateUrl: './neurologico1.page.html',
  styleUrls: ['./neurologico1.page.scss'],
})
export class Neurologico1Page implements OnInit {
  cont:number=0;
  neuro1:string="";
  public form = [
    { isChecked: false, item:'está somnoliento cuando nadie lo molesta. ' },
    { isChecked: false, item:'responde solo a estímulos verbales. ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }

  constructor(private router: Router) { }

  regreso1(){
    this.router.navigate(['/neurologico2'])
  }

  adelante3(){
    this.router.navigate(['/neurologico0'])
  }

  ngOnInit() {
  }

  Change(){  
    this.cont = 0;
    this.neuro1=""; 
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.neuro1=this.neuro1+this.form[x].item;
      }
    }
    
    if(this.cont>0){
     this.parametros.valor=1;
     this.parametros.pregunta="El paciente: "+this.neuro1;
     sessionStorage.setItem("Pregunta_neurologico", JSON.stringify(this.parametros));
     this.router.navigate(['/cardiovascular4']);
   }
  }
}

