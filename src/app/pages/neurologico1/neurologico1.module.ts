import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Neurologico1PageRoutingModule } from './neurologico1-routing.module';

import { Neurologico1Page } from './neurologico1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Neurologico1PageRoutingModule
  ],
  declarations: [Neurologico1Page]
})
export class Neurologico1PageModule {}
