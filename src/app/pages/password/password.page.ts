import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-password',
  templateUrl: './password.page.html',
  styleUrls: ['./password.page.scss'],
})
export class PasswordPage implements OnInit {
  contra = {
      nueva: "",
      confirmacion: "",
    }

  constructor(private router: Router,public alertCtrl: AlertController) { }

  ngOnInit() {
  }

  async Cambiar(){
    var ans = this.contra.nueva.localeCompare(this.contra.confirmacion); 
    if (ans == 0) { //ans = 0 si las contraseñas son iguales
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Restablecer Contraseña',
        subHeader:'Contraseña Restablecida',
        message: 'Su contraseña ha sido restablecida.',
        buttons: ['OK']
     });
     await alert.present();
     this.router.navigate(['/iniciar-sesion']);
    } else { 
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Restablecer Contraseña',
        subHeader:'Error al restablecer contraseña',
        message: 'Verifique que las contraseñas sean iguales.',
        buttons: ['OK']
     });
     await alert.present();
    } 
}

}
