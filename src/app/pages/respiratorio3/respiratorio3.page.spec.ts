import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Respiratorio3Page } from './respiratorio3.page';

describe('Respiratorio3Page', () => {
  let component: Respiratorio3Page;
  let fixture: ComponentFixture<Respiratorio3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Respiratorio3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Respiratorio3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
