import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Respiratorio3PageRoutingModule } from './respiratorio3-routing.module';

import { Respiratorio3Page } from './respiratorio3.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Respiratorio3PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Respiratorio3Page]
})
export class Respiratorio3PageModule {} 
