import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Cardiovascular4Page } from './cardiovascular4.page';

const routes: Routes = [
  {
    path: '',
    component: Cardiovascular4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Cardiovascular4PageRoutingModule {}
