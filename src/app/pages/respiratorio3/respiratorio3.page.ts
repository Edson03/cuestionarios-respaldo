import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { PopinfoComponent } from '../../components/popinfo/popinfo.component';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-respiratorio3',
  templateUrl: './respiratorio3.page.html',
  styleUrls: ['./respiratorio3.page.scss'],
})
export class Respiratorio3Page implements OnInit {
  public form = [
    { isChecked: false, item:'Trabajo respiratorio severo. ' },
    { isChecked: false, item:'Oxígeno en mascarilla con reservorio o mayor de 3 L/min. ' },
    { isChecked: false, item:'Apnea. ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }
  cont:number=0;
  resp3:string="";

  constructor(public alertCtrl: AlertController,private router: Router, private popoverCtrl: PopoverController ) { }

  ngOnInit() {
  }

  async mostrarPop(evento ) {
    const popover = await this.popoverCtrl.create({
      component: PopinfoComponent,
      event: evento,
      mode: 'ios',
    });
    await popover.present();
  }

  Change(){  
    this.cont = 0;
    this.resp3=""; 
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.resp3=this.resp3+this.form[x].item;
      }
    }

    if(this.cont>0){
      let Storage = JSON.parse(sessionStorage.getItem("Pregunta_respiratoria"));
       this.parametros.valor=3;
      this.parametros.pregunta=Storage.pregunta+" "+this.resp3;
      sessionStorage.setItem("Pregunta_respiratoria", JSON.stringify(this.parametros));
      this.router.navigate(['/preguntas-extras']);
     }
  }

  async Siguiente(){
    let respiratorio=JSON.parse(sessionStorage.getItem("Pregunta_respiratoria"));
    if(respiratorio.valor==2){
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Alerta',
        subHeader:'Evaluación Respiratoria',
        message: 'El paciente presenta un estado Respiratorio MODERADO debido a su Frecuencia Respiratoria o Saturación y Uso de Oxígeno por lo que se procederá con la siguiente evaluación.',
        buttons: ['OK']
     });
     await alert.present();
      this.router.navigate(['/preguntas-extras']);
    }else{
    this.router.navigate(['/respiratorio2']);
    }
  }
}