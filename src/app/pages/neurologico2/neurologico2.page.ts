import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-neurologico2',
  templateUrl: './neurologico2.page.html',
  styleUrls: ['./neurologico2.page.scss'],
})
export class Neurologico2Page implements OnInit {
  cont:number=0;
  neuro2:string="";
  public form = [
    { isChecked: false, item:'está irritable. ' },
    { isChecked: false, item:'es difícil de consolar. ' },
    { isChecked: false, item:'responde solo a estímulos dolorosos. ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }

  constructor(private router: Router) { }
  adelante(){
    this.router.navigate(['/neurologico1'])
  }
  regreso(){
    this.router.navigate(['/neurologico3'])
  }

  ngOnInit() {
  }

  Change(){  
    this.cont = 0;
    this.neuro2=""; 
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.neuro2=this.neuro2+this.form[x].item;
      }
    }

    if(this.cont>0){
      this.parametros.valor=2;
     this.parametros.pregunta="El paciente: "+this.neuro2;
     sessionStorage.setItem("Pregunta_neurologico", JSON.stringify(this.parametros));
     this.router.navigate(['/cardiovascular4']);
   }
  }

}
