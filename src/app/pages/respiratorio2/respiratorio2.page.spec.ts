import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Respiratorio2Page } from './respiratorio2.page';

describe('Respiratorio2Page', () => {
  let component: Respiratorio2Page;
  let fixture: ComponentFixture<Respiratorio2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Respiratorio2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Respiratorio2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
