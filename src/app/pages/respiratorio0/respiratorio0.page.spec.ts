import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Respiratorio0Page } from './respiratorio0.page';

describe('Respiratorio0Page', () => {
  let component: Respiratorio0Page;
  let fixture: ComponentFixture<Respiratorio0Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Respiratorio0Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Respiratorio0Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
