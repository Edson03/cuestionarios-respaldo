import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { ModalController, AlertController } from '@ionic/angular';
import { ModalPopupPage } from '../../modal-popup/modal-popup.page';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';



@Component({
  selector: 'app-iniciar-sesion',
  templateUrl: './iniciar-sesion.page.html',
  styleUrls: ['./iniciar-sesion.page.scss'],
})
export class IniciarSesionPage implements OnInit {

  user:Boolean=false;
  usuarioDB:any[]=[];
  dataLlamada:any[]=[];

  usuario = {
    username : '',
    password : '',
  };

  personal ={
    id : 0,
    tipo_de_persona : 0,
  }

  key='Evat2020Evat';


  Encriptacion: string;
  Desencriptacion: string;


  constructor(public modalController: ModalController, public http: HttpClient,public alertCtrl: AlertController,private router: Router) { }

  ngOnInit() {
  }

  refresh() {
    console.log(this.usuario);

    this.MetodoGETPersonas();
    

 


  //  this.Encriptacion=CryptoJS.AES.encrypt(this.usuario.password.trim(),this.key.trim()).toString();
  //  console.log(this.Encriptacion,this.usuario.username);
   // this.Desencriptacion=CryptoJS.AES.decrypt(this.Encriptacion.trim(),this.key.trim()).toString(CryptoJS.enc.Utf8);
   // console.log(this.Desencriptacion,this.usuario.username);
  }

MetodoGETPersonas() {
     this.http.get<any[]>(`https://localhost:44347/api/personas/${this.usuario.username}`).subscribe(dataPersonas=>{
    
      //this.dataLlamada=dataPersonas;
      
     // console.log(peopleArray);
      //console.log(Object.values(this.dataLlamada))
     // console.log(Object.values)
      //console.log(dataPersonas);
      if(dataPersonas==null){
         console.log('No se encontro ningun usuario con ese nombre');
        this.UsuarioNoRegistrado();
      }else{
        const peopleArray = Object.values(dataPersonas)
        console.log(peopleArray);
        console.log(peopleArray[5])
        console.log(CryptoJS.AES.decrypt(peopleArray[5].trim(),this.key.trim()).toString(CryptoJS.enc.Utf8))
        
        if(this.usuario.password==CryptoJS.AES.decrypt(peopleArray[5].trim(),this.key.trim()).toString(CryptoJS.enc.Utf8))
        {
          console.log('Si es igual la contrase;a')
          this.personal.id=peopleArray[0];
          this.personal.tipo_de_persona=peopleArray[1];
          sessionStorage.setItem("Informacion_Personal", JSON.stringify(this.personal));
          this.router.navigate(['/datos-pacientes']);
        }else{
          this.PasswordIncorrecto();
          console.log('No coinciden las contrase;as')
        }

      }
        
      
    });
  }

 async UsuarioNoRegistrado(){
    const alert = await this.alertCtrl.create({
      backdropDismiss: false,
      header: 'Inicio de Sesión',
      subHeader:'Usuario Incorrecto',
      message: 'El nombre del usuario es incorrecto, por favor verifique su nombre de usuario.',
      buttons: ['Aceptar']
   });
   await alert.present();
   this.usuario.password='';
   this.usuario.username='';
  }

  async PasswordIncorrecto(){
    const alert = await this.alertCtrl.create({
      backdropDismiss: false,
      header: 'Inicio de Sesión',
      subHeader:'Contraseña Incorrecta',
      message: 'La contraseña ingresada es incorrecta por favor verifique su contraseña.',
      buttons: ['Aceptar']
   });
   await alert.present();
   this.usuario.password='';
   this.usuario.username='';
  }

  async showModal() {
    console.log('estoy aqui');
    const modal = await this.modalController.create({
      component: ModalPopupPage,
      componentProps: {
        'name': 'Hello User'
      }
    });
    return await modal.present();
  }

  onSubmitTemplate() {
    console.log('Form submit');
   
  }

}
