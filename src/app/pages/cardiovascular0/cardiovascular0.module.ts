import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Cardiovascular0PageRoutingModule } from './cardiovascular0-routing.module';

import { Cardiovascular0Page } from './cardiovascular0.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Cardiovascular0PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Cardiovascular0Page]
})
export class Cardiovascular0PageModule {}
