import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Cardiovascular4Page } from './cardiovascular4.page';

describe('Cardiovascular4Page', () => {
  let component: Cardiovascular4Page;
  let fixture: ComponentFixture<Cardiovascular4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Cardiovascular4Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Cardiovascular4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
