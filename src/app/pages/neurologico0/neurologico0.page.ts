import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-neurologico0',
  templateUrl: './neurologico0.page.html',
  styleUrls: ['./neurologico0.page.scss'],
})
export class Neurologico0Page implements OnInit {
  cont:number=0;  valor:number=0;
  neuro:string="";
  public form = [
    { isChecked: false, item:'está alerta o durmiendo apropiadamente. ' },
    { isChecked: false, item:'está en su estado basal de alerta. ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }
   auxneuro: any;
   auxneuro1: any;
   auxneuro2: any;
   auxneuro3: any;

  constructor(private router: Router,public alertCtrl: AlertController) {
   }

  regreso2(){
    this.router.navigate(['/neurologico1'])
  }

  ngOnInit() {
  }

  Change(){  
    this.cont = 0;
    this.neuro=""; 
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.neuro=this.neuro+this.form[x].item;
      }
    }
    
    if(this.cont>0){
      this.parametros.valor=0;
     this.parametros.pregunta="El paciente: "+this.neuro;
     sessionStorage.setItem("Pregunta_neurologico", JSON.stringify(this.parametros));
     this.router.navigate(['/cardiovascular4']);
   }
  }

  async Siguiente(){
    const alert = await this.alertCtrl.create({
      backdropDismiss: false,
      header: 'Alerta',
      subHeader: 'Usted no selecciono ninguna opción',
      message: 'Por favor, seleccione una opción de la evaluación Neurológica.',
      buttons: ['OK']
    });
    await alert.present();
  }

}
