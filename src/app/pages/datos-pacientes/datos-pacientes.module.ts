import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatosPacientesPageRoutingModule } from './datos-pacientes-routing.module';

import { DatosPacientesPage } from './datos-pacientes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatosPacientesPageRoutingModule
  ],
  declarations: [DatosPacientesPage]
})
export class DatosPacientesPageModule {}
