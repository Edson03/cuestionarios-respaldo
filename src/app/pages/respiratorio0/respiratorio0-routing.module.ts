import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Respiratorio0Page } from './respiratorio0.page';

const routes: Routes = [
  {
    path: '',
    component: Respiratorio0Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Respiratorio0PageRoutingModule {}
