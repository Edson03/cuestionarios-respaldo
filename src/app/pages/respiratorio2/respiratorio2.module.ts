import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Respiratorio2PageRoutingModule } from './respiratorio2-routing.module';

import { Respiratorio2Page } from './respiratorio2.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Respiratorio2PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Respiratorio2Page]
})
export class Respiratorio2PageModule {}
