import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Respiratorio1Page } from './respiratorio1.page';

const routes: Routes = [
  {
    path: '',
    component: Respiratorio1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Respiratorio1PageRoutingModule {}
