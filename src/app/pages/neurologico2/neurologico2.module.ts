import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Neurologico2PageRoutingModule } from './neurologico2-routing.module';

import { Neurologico2Page } from './neurologico2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Neurologico2PageRoutingModule
  ],
  declarations: [Neurologico2Page]
})
export class Neurologico2PageModule {}
