import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Respiratorio0PageRoutingModule } from './respiratorio0-routing.module';

import { Respiratorio0Page } from './respiratorio0.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Respiratorio0PageRoutingModule
  ],
  declarations: [Respiratorio0Page]
})
export class Respiratorio0PageModule {}
