import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';



@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.page.html',
  styleUrls: ['./resultados.page.scss'],
})
export class ResultadosPage implements OnInit {
   Envio:boolean=true;
   dataLlamada:any[]=[];
   dataPersonas:any[]=[];
   Medico_Acargo:any[]=[];
   neurologico: any;
   cardiovascular: any;
   respiratorio: any;
   preocupacion: any;
   total:number=0;
   proximo:string="";
   /* Variables de id y datos de pacientes*/
   Informacion_Personal:any;
   Paciente:any;


  constructor(private router: Router,public alertCtrl: AlertController,public http: HttpClient) {
    this.GetStorage();
    this.data[0].value=this.neurologico.value;
    this.data[1].value=this.cardiovascular.value;
    this.data[2].value=this.respiratorio.value;
    this.data[3].value=this.preocupacion.value;
    this.data[4].value=this.total;
    
   }
  data = [
    {
      label: 'Neurológico',
      type: 'number',
      value: 0
    },
    {
      label: 'Cardiovascular',
      type: 'number',
      value: 0
    },
    {
      label: 'Respiratorio',
      type: 'number',
      value: 0
    },
    {
      label: 'Preocupación',
      type: 'number',
      value: 0
    },
    {
      label: 'Total',
      type: 'number',
      value: 0
    }
  ]
  
  ngOnInit() {
    //Se colocaron las instrucciones al iniciar la pagina
    this.GetStorage();
    this.data[0].value=this.neurologico.valor;
    this.data[1].value=this.cardiovascular.valor;
    this.data[2].value=this.respiratorio.valor;
    this.data[3].value=this.preocupacion.valor;
    this.data[4].value=this.total;
    this.ObtenerMedicoAcargo();
    this.EnviarDatos()
   // this.Resultados(); Este metodo se movio a la logica de ObtenerMedicoAcargo
    
  }

  GetStorage(){
    this.Paciente=JSON.parse(sessionStorage.getItem("Paciente"));
    this.Informacion_Personal=JSON.parse(sessionStorage.getItem("Informacion_Personal"));
    this.neurologico=JSON.parse(sessionStorage.getItem("Pregunta_neurologico"));
    this.cardiovascular=JSON.parse(sessionStorage.getItem("Pregunta_cardiovascular"));
    this.respiratorio=JSON.parse(sessionStorage.getItem("Pregunta_respiratoria"));
    this.preocupacion=JSON.parse(sessionStorage.getItem("Pregunta_preocupacion"));
    this.total=this.neurologico.valor+this.cardiovascular.valor+this.respiratorio.valor+this.preocupacion.valor;
  }
  
  Siguiente(){
    //Limpiamos las variables de Sesion Utilizadas
    sessionStorage.clear();
    this.router.navigate(['/datos-pacientes']);
  }


  EnviarDatos() {
    console.log(this.neurologico)
    console.log(this.respiratorio)
    console.log(this.cardiovascular)
    this.Envio=false;


    let postData = {
      "ID_persona" : this.Informacion_Personal.id, // Hay que cambiar este valor por el id_persona que es el valor de la tabla personas que se registra cuando se inicia sesion
      "Nombre_Paciente" : this.Paciente.nombre,
      "Registro_Paciente" : (this.Paciente.registro).toString(),
      "Formato_Edad" : (this.Paciente.tipoedad).toString(),
      "Edad_Paciente" : (this.Paciente.edad).toString(),
      "Pregunta_neurologico" : this.neurologico.pregunta,
      "Valor_neurologico" : this.neurologico.valor,
      "Pregunta_cardiovascular" : this.cardiovascular.pregunta,
      "Valor_cardiovascular" : this.cardiovascular.valor,
      "Pregunta_respiratoria" : this.respiratorio.pregunta,
      "Valor_respiratorio" : this.respiratorio.valor,
      "Tipo_preocupacion" : this.preocupacion.pregunta,
      "Valor_preocupacion" : this.preocupacion.valor,
      "total" : this.total
    }
    console.log('Estos valores se van a enviar ',postData);
    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );

    this.http.post('https://localhost:44347/api/cuestionario', postData, { headers: headers}).subscribe()


  }

  async Resultados(nombre,telefono){
    if(this.total<=2){
      this.proximo="Continuar la evaluación rutinaria.";
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Resultados del paciente',
        subHeader:'Realizar evaluación cada 3 hrs.',
        message: 'El paciente presenta un total de '+this.total+" puntos. Continuar con la evaluación rutinaria.",
        buttons: ['OK']
     });
     await alert.present();
    }else if (this.total>=3 && this.total<=4){
      this.proximo="Realizar evaluación cada 3 horas."
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Resultados del paciente',
        subHeader:'Realizar evaluación cada 3 hrs.',
        message: 'El paciente presenta un total de '+this.total+" puntos. Debe considerar consultar a Médico de Cuidado Intensivo. Medico Acargo: "+nombre+" Número Telefonico : "+telefono,
        buttons: ['OK']
     });
     await alert.present();
    }else if (this.total>=5){
      this.proximo="Realizar evaluación cada hora."
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Resultados del paciente',
        subHeader:'Realizar evaluación cada hora.',
        message: 'El paciente presenta un total de '+this.total+" puntos. Debe consultar al Medico de Cuidado Intensivo. Medico Acargo: "+nombre+" Número Telefonico : "+telefono,
        buttons: ['LLAMAR']
     });
     await alert.present();
    }
  }

  ObtenerMedicoAcargo(){// Este metodo obtiene los medicos acargo del intensivo, nos proporciona el nombre y numero telefonico de cada uno
    this.http.get<any[]>('https://localhost:44347/api/llamada').subscribe(dataLlamada=>{
    
      this.dataLlamada=dataLlamada;
      console.log('Este es el contenido de la tabla LLAMADA',dataLlamada);
      console.log(this.dataLlamada.length);
      if(dataLlamada.length==0){
        console.log('NO SE ENCUENTRO NINGUN DOCTOR EN EL ROL DE LLAMADA');
        this.PrecaucionNoHayMedicos();
      }else{
        console.log('YA HAY DOCTORES EN LLAMADA!!!')

        this.http.get<any[]>('https://localhost:44347/api/personas').subscribe(dataPersonas=>{
    
          this.dataPersonas=dataPersonas;
          console.log('Este es el contenido de la tabla Personas',dataPersonas);
          console.log(this.dataPersonas.length);

          for (let doctor_llamada of this.dataLlamada) {
            var nombre="";
            var telefono="";
            for (let doctores of this.dataPersonas) {
              if(doctor_llamada.id_persona==doctores.id){
                  console.log('Se encontro un doctor en llamada')
                  nombre=doctores.nombres;
                  telefono=doctores.numero_telefonico;
                  console.log(nombre,telefono);
                  this.Resultados(nombre,telefono);
              }
            }
            
          } 
    
    
        });


      }
    });

  }

  async PrecaucionNoHayMedicos(){
    if(this.total<=2){
      this.proximo="Continuar la evaluación rutinaria.";
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Resultados del paciente',
        subHeader:'Realizar evaluación cada 3 hrs.',
        message: 'El paciente presenta un total de '+this.total+" puntos. Continuar con la evaluación rutinaria. NOTA: No se encuentra ningun Doctor en rol de llamada.",
        buttons: ['OK']
     });
     await alert.present();
    }else if (this.total>=3 && this.total<=4){
      this.proximo="Realizar evaluación cada 3 horas."
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Resultados del paciente',
        subHeader:'Realizar evaluación cada 3 hrs.',
        message: 'El paciente presenta un total de '+this.total+" puntos. Debe considerar consultar a Médico de Cuidado Intensivo. NOTA: No se encuentra ningun Doctor en rol de llamada. ",
        buttons: ['OK']
     });
     await alert.present();
    }else if (this.total>=5){
      this.proximo="Realizar evaluación cada hora."
      const alert = await this.alertCtrl.create({
        backdropDismiss: false,
        header: 'Resultados del paciente',
        subHeader:'Realizar evaluación cada hora.',
        message: 'El paciente presenta un total de '+this.total+" puntos. Debe consultar al Medico de Cuidado Intensivo. NOTA: No se encuentra ningun Doctor en rol de llamada. ",
        buttons: ['ACEPTAR']
     });
     await alert.present();
    }
  }

}
