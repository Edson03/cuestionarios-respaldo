import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Neurologico0Page } from './neurologico0.page';

const routes: Routes = [
  {
    path: '',
    component: Neurologico0Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Neurologico0PageRoutingModule {}
