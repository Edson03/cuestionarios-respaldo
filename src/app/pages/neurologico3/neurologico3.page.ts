import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-neurologico3',
  templateUrl: './neurologico3.page.html',
  styleUrls: ['./neurologico3.page.scss'],
})
export class Neurologico3Page implements OnInit {
  cont:number=0;
  neuro3:string="";
  public form = [
    { isChecked: false, item:'está letárgico. ' },
    { isChecked: false, item:'está confundido. ' },
    { isChecked: false, item:'está sin fuerzas, ' },
    { isChecked: false, item:'no responde a estímulos. ' },
    { isChecked: false, item:'presenta convulsiones. ' },
    { isChecked: false, item:'presenta anisocoria. ' },
    { isChecked: false, item:'presenta pupilas no reactivas a la luz. ' },
  ];
  parametros={
    pregunta:'',
    valor:0,
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }

  Change(){
    this.cont = 0;
    this.neuro3="";
    for (var x=0; x < this.form.length; x++) {
      if (this.form[x].isChecked==true) {
        this.cont += 1;
        this.neuro3=this.neuro3+this.form[x].item;
      }
    }
    if(this.cont>0){
       this.parametros.valor=3;
      this.parametros.pregunta="El paciente: "+this.neuro3;
      sessionStorage.setItem("Pregunta_neurologico", JSON.stringify(this.parametros));
      this.router.navigate(['/cardiovascular4']);
    }
  }

  Siguiente(){
    this.router.navigate(['/neurologico2']);
  }

}

