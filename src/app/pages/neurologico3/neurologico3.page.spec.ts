import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Neurologico3Page } from './neurologico3.page';

describe('Neurologico3Page', () => {
  let component: Neurologico3Page;
  let fixture: ComponentFixture<Neurologico3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Neurologico3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Neurologico3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
