import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Neurologico3PageRoutingModule } from './neurologico3-routing.module';

import { Neurologico3Page } from './neurologico3.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Neurologico3PageRoutingModule,
    ComponentsModule
  ],
  declarations: [Neurologico3Page]
})
export class Neurologico3PageModule {}
