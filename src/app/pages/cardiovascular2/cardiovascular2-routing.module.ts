import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Cardiovascular2Page } from './cardiovascular2.page';

const routes: Routes = [
  {
    path: '',
    component: Cardiovascular2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Cardiovascular2PageRoutingModule {}
