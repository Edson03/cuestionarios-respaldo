import { Component, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { stringify } from 'querystring';

@Component({
  selector: 'app-datos-pacientes',
  templateUrl: './datos-pacientes.page.html',
  styleUrls: ['./datos-pacientes.page.scss'],
})
export class DatosPacientesPage implements OnInit {
  paciente= {
    nombre:'',
    registro:'',
    tipoedad:'',
    edad: '',

  };

  constructor(public alertCtrl: AlertController,private router: Router) { }

  ngOnInit() {
  }

  async Cambios(){
    if (this.paciente.tipoedad=="meses"){
      if(parseInt(this.paciente.edad)>23){
        this.paciente.edad="";
        const alert = await this.alertCtrl.create({
          backdropDismiss: false,
          header: 'Alerta',
          subHeader:'Selecciono formato Meses',
          message: 'Debe ingresar un valor entre el rango 1 a 23.',
          buttons: ['OK']
       });
       await alert.present();
      }
    } 

  }

  Siguiente(){
    sessionStorage.setItem("Paciente", JSON.stringify(this.paciente));
    this.router.navigate(['/neurologico3'])
  }


}
